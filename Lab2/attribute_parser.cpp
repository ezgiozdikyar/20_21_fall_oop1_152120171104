#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;
string extractString(string str, int choice)
{
    string tempStr;
    stringstream stream;
    stream << str;
    stream >> tempStr;
    if (tempStr[tempStr.size() - 1] == '>')
    {
        tempStr.erase(tempStr.begin() + tempStr.size() - 1);
    }
    tempStr.erase(tempStr.begin());
    if (choice == 1)
    {
        tempStr.erase(tempStr.begin() + tempStr.size() - 1);
    }
    return tempStr;
}
string parseString(vector<string>v, vector<string>q)
{
    stringstream stream;
    string query, tempStr, attribute, value;
    int j = 0, i = 0, control = 0;
    for (i = 0; i < q.size() - 1; i++)
    {
        query.clear();
        query = *(q.begin() + i);
        for (; j < v.size(); j++)
        {
            tempStr.clear();
            tempStr = v[j];
            if (tempStr[1] == '/')
            {
                control--;
                continue;
            }
            if (query == extractString(tempStr, 0))
            {
                if (control == 0)
                {
                    j++;
                    break;
                }
                else
                {
                    return "Not Found!";
                }
            }
            else
            {
                control++;
            }
        }
        if (j == v.size())
        {
            return "Not Found!";
        }
    }
    if (i == q.size() - 1)
    {
        char ch;
        stream.str("");
        stream << v[j - 1];
        while (stream >> attribute)
        {
            if (attribute == q[i])
            {
                stream >> ch;
                stream >> value;
                return extractString(value, 1);
            }
        }
    }
    return "Not Found!";
}
vector<string> seperateQuery(string query)
{
    vector<string>v;
    stringstream stream;
    string tempStr;
    for (int i = 0; i < query.size(); i++)
    {
        if (query[i] == '.' || query[i] == '~')
        {
            query[i] = ' ';
        }
    }
    stream << query;
    while (stream >> tempStr)
    {
        v.push_back(tempStr);
    }
    return v;
}
int main()
{
    vector<string>inputs;
    string tempStr, query;
    int line, queries;
    cin >> line >> queries;
    for (int i = 0; i <= line; i++)
    {
        getline(cin, tempStr);
        if (i == 0)
        {
            tempStr.clear();
            continue;
        }
        inputs.push_back(tempStr);
        tempStr.clear();
    }
    vector<string>q;
    for (int i = 0; i < queries; i++)
    {
        cin >> query;
        q = seperateQuery(query);
        cout << parseString(inputs, q) << endl;
        query.clear();
        q.clear();
    }
    return 0;
}