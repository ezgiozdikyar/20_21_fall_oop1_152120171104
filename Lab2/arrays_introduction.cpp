#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
int main() {
    int* array;
    int number;
    cin >> number;
    array = new int[number];
    for (int i = 0; i < number; i++)
        cin >> array[i];
    for (int i = number - 1; i >= 0; i--)
        cout << array[i] << " ";
    return 0;
}