#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
int main() 
{
    int size, query, k,i,j;
    cin >> size >> query;
    int** arr1 = new int* [size];
    for (int i = 0; i < size; i++) 
    {
        cin >> k;
        arr1[i] = new int[k];
        for (int j = 0; j < k; j++) 
        {
            cin >> arr1[i][j];
        }
    }
    for (int m = 0; m < query; m++) 
    {
        cin >> i >> j;
        cout << arr1[i][j] << endl;
    }
    return 0;
}