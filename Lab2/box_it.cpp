#include<bits/stdc++.h>
using namespace std;
class Box{
    private:
    int l,b,h;
    public:
    Box()
    {
        l=0;
        b=0;
        h=0;
    }
    Box(int length, int breadth, int height)
    {
        this->l=length;
        this->b=breadth;
        this->h=height;
    }
    Box(const Box &b1)
    {
        this->l=b1.l;
        this->b=b1.b;
        this->h=b1.h;
    }
    int getLength(){return this->l;};
    int getBreadth(){return this->b;};
    int getHeight(){return this->h;};
    long long CalculateVolume(){return (long long)l*b*h;};
    friend bool operator <(Box& b1,Box& b2)
    {
        if( (b1.l < b2.l) || ((b1.b < b2.b) && (b1.l == b2.l)) || ((b1.h < b2.h) &&    (b1.l == b2.l) && (b1.b == b2.b)) )
        return true;
        else
        return false;
    };
    friend ostream &operator<< (ostream& out, Box const &B);
};
ostream &operator<< (ostream& out, const Box &B)
{
        out<<B.l<<" "<<B.b<<" "<<B.h;
        return out;
}
void check2()
{
	int n;
	cin>>n;
	Box temp;
	for(int i=0;i<n;i++)
	{
		int type;
		cin>>type;
		if(type ==1)
		{
			cout<<temp<<endl;
		}
		if(type == 2)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			temp=NewBox;
			cout<<temp<<endl;
		}
		if(type==3)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			if(NewBox<temp)
			{
				cout<<"Lesser\n";
			}
			else
			{
				cout<<"Greater\n";
			}
		}
		if(type==4)
		{
			cout<<temp.CalculateVolume()<<endl;
		}
		if(type==5)
		{
			Box NewBox(temp);
			cout<<NewBox<<endl;
		}

	}
}
int main()
{
	check2();
}