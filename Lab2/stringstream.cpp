#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
vector<int> parseInts(string str)
{
	vector<int> v;
    int numbers;
    stringstream str2(str);
    for(;str2>>numbers;)
    {
        v.push_back(numbers);
        if(str2.peek()==',')
        {
          str2.ignore();
        }
    }
    return v;
}
int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++)
    {
        cout << integers[i] << "\n";
    }
    return 0;
}
