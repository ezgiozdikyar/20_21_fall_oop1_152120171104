#include <iostream>
#include <string>
using namespace std;
int main() {
    string s1, s2;
    char c1, c2;
    getline(cin, s1);
    getline(cin, s2);
    cout << s1.size() << " " << s2.size() << endl;
    cout << s1 + s2 << endl;
    c1 = s1[0];
    c2 = s2[0];
    s2[0] = c1;
    s1[0] = c2;
    cout << s1 << " " << s2 << endl;
    return 0;
}